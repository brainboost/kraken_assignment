FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1

RUN mkdir /home/kraken_assignment

WORKDIR /home/kraken_assignment
RUN cp -r * /home/kraken_assignment/
COPY . /home/kraken_assignment


RUN pip install pymongo
CMD ["python", "read_transactions.py"]