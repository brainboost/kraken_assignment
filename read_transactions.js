'use strict';

const fs = require('fs');
const client = require('mongodb');


var deposit_is_valid_when_transactions = 6;
var path = '/home/kraken_assignment/';
var dbname = 'transactions';



function parse_json_from_file(path){
    let rawdata = fs.readFileSync(path);
    let data = JSON.parse(rawdata);
    console.log(data);
    return data;
}


function json_to_mongo(collection_name,json){
      const uri = "mongodb+srv://kraken_assignment_mongo_1/test?retryWrites=true&w=majority";
 

    const client = new MongoClient(uri);
 
    try {
        // Connect to the MongoDB cluster
        await client.connect();
 
        // Make the appropriate DB calls
        await  listDatabases(client);
 
    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }


}



def json_to_mongo(collection_name,json):
    try:
        collection = client[dbname].create_collection(collection_name)
        for doc in json:
            collection.insert_one(dict(doc))
    except:
        print("error reading file")
        pass
    
    
    

def parse_transactions_from_json_files():
    parsed_json_1 = parse_json_from_file(path + 'transactions-1.json')
    parsed_json_2 = parse_json_from_file(path + 'transactions-2.json')
    transactions = parsed_json_1['transactions']+parsed_json_2['transactions']
    return transactions


function parse_transactions_from_json_files(){


}




def parse_customers_from_json_files():
    return parse_json_from_file(path + 'customers.json')


function parse_customers_from_json_files(){

    
}








async function main() {
    
    let pipeline = [
    {   
        '$lookup': {
                    'from': 'transactions',
                    'localField' : 'wallet_addr',
                    'foreignField' : 'address',
                    'as' : 'transactions_from_customer'
        }
    },
    {
        '$match': {
            'transactions_from_customer.confirmations': {
                '$gte': deposit_is_valid_when_transactions
            }
        }
    }
];

let customers = json_to_mongo('customers',parse_customers_from_json_files());
let transactions = json_to_mongo('transactions',parse_transactions_from_json_files());



    
    

    
customers_with_their_valid_deposits_confirmations_gte = list(client['transactions']['customers'].aggregate(pipeline))

customers_with_their_valid_deposits_confirmations_gte_addresses = [vd['wallet_addr'] for vd in list(customers_with_their_valid_deposits_confirmations_gte)]

all_transactions = list(client['transactions']['transactions'].find({}))

addresses_from_all_transactions = set([o['address'] for o in all_transactions])  # remove duplicates

other_customer_transactions = [x for x in all_transactions if x['address'] not in customers_with_their_valid_deposits_confirmations_gte_addresses]




for customer in customers_with_their_valid_deposits_confirmations_gte:
    print('Deposited for ' + customer['name'] + ': count=' + str(len(customer['transactions_from_customer']))  + ' amount sum=' + str(sum([am['amount'] for am in customer['transactions_from_customer']])))

other_customer_transactions_count = len(other_customer_transactions)
other_customer_transactions_amount_sum = sum([x['amount'] for x in other_customer_transactions])

print("Deposited without reference: count=" + str(other_customer_transactions_count) + " sum=" + str(other_customer_transactions_amount_sum))


all_valid_deposit_amounts =  [x['amount'] for x in all_transactions if x['confirmations'] >= deposit_is_valid_when_transactions]
print("Smallest valid deposit: " + str(min(all_valid_deposit_amounts)))
print("Largest valid deposit: " + str(min(all_valid_deposit_amounts)))





}





// db.getCollection('customers').aggregate([

//     {
//         $lookup: {
//                 from: 'transactions',
//                 localField: 'wallet_addr',
//                 foreignField: 'address',
//                 as: 'transactions_from_customer'
//             }
//      },
//      {  $match : {
//                     'transactions_from_customer.confirmations' : {
//                                                                     $gte: 6

//                                                                    }
//                  }
//      }
      
// ])
