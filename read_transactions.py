import json
import pymongo


deposit_is_valid_when_transactions = 6
path = '/home/kraken_assignment/'
dbname = 'transactions'
client = pymongo.MongoClient('kraken_assignment_mongo_1', 27017)


def parse_json_from_file(path):
    with open(path) as f1:
        lines = f1.readlines()
    lines = json.loads(lines[0])
    return lines

def json_to_mongo(collection_name,json):
    try:
        collection = client[dbname].create_collection(collection_name)
        for doc in json:
            collection.insert_one(dict(doc))
    except:
        print("error reading file")
        pass
    
    
    

def parse_transactions_from_json_files():
    parsed_json_1 = parse_json_from_file(path + 'transactions-1.json')
    parsed_json_2 = parse_json_from_file(path + 'transactions-2.json')
    transactions = parsed_json_1['transactions']+parsed_json_2['transactions']
    return transactions



def parse_customers_from_json_files():
    return parse_json_from_file(path + 'customers.json')




customers = json_to_mongo('customers',parse_customers_from_json_files())
transactions = json_to_mongo('transactions',parse_transactions_from_json_files())



    
    
pipeline = [
    {   
        '$lookup': {
                    'from': 'transactions',
                    'localField' : 'wallet_addr',
                    'foreignField' : 'address',
                    'as' : 'transactions_from_customer'
        }
    },
    {
        '$match': {
            'transactions_from_customer.confirmations': {
                '$gte': deposit_is_valid_when_transactions
            }
        }
    }
]    
    
customers_with_their_valid_deposits_confirmations_gte = list(client['transactions']['customers'].aggregate(pipeline))

customers_with_their_valid_deposits_confirmations_gte_addresses = [vd['wallet_addr'] for vd in list(customers_with_their_valid_deposits_confirmations_gte)]

all_transactions = list(client['transactions']['transactions'].find({}))

addresses_from_all_transactions = set([o['address'] for o in all_transactions])  # remove duplicates

other_customer_transactions = [x for x in all_transactions if x['address'] not in customers_with_their_valid_deposits_confirmations_gte_addresses]




for customer in customers_with_their_valid_deposits_confirmations_gte:
    print('Deposited for ' + customer['name'] + ': count=' + str(len(customer['transactions_from_customer']))  + ' amount sum=' + str(sum([am['amount'] for am in customer['transactions_from_customer']])))

other_customer_transactions_count = len(other_customer_transactions)
other_customer_transactions_amount_sum = sum([x['amount'] for x in other_customer_transactions])

print("Deposited without reference: count=" + str(other_customer_transactions_count) + " sum=" + str(other_customer_transactions_amount_sum))


all_valid_deposit_amounts =  [x['amount'] for x in all_transactions if x['confirmations'] >= deposit_is_valid_when_transactions]
print("Smallest valid deposit: " + str(min(all_valid_deposit_amounts)))
print("Largest valid deposit: " + str(min(all_valid_deposit_amounts)))




# db.getCollection('customers').aggregate([

#     {
#         $lookup: {
#                 from: 'transactions',
#                 localField: 'wallet_addr',
#                 foreignField: 'address',
#                 as: 'transactions_from_customer'
#             }
#      },
#      {  $match : {
#                     'transactions_from_customer.confirmations' : {
#                                                                     $gte: 6

#                                                                    }
#                  }
#      }
      
# ])
